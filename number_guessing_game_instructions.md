'''Number Guessing Game instructions'''

The goal of this program is to create a game that allows the user to make guesses of a number and the program compares this to a previous created random number until the user runs out of guess or guesses correctly. All this will be displayed on the command line as a more advance graphical output is beyond the scope of this basic introduction.

The order to create each line in this program can be done in many different ways depending on one's methodology or preference. This method is by no means the only or best method but was chosen to be easiest to teach to others with preference to starting with simpler concepts and working toward more complicated. The example finish program may also differ very slightly do to the chosen method of instructing, but all line and concepts are covered.

To start of with a simple concept, let first greet the player. Displaying or 'print'ing text to a command line is one of the earliest and simplest concept in python and only requires a single word and a couple brackets:

	print()

Anything placed inside the brackets will be printed to the command line. However, to display a string of text, this text must be within single or double quotes (the same must be used on each end of the text). So to give the player a greeting:

	print('Hello! Welcome to the game of guesses!')

Try running this program now and inside a command line you should see the message between the quotes.

Next, we should find out if the player actually wants to play this guessing game. This will take explain two concepts, taking input from the command line, and storing that input some where. First, to accept input from the command line we use a syntax that is very similiar to the print syntax with one extra feature. This is the 'input' syntax. Just like print, input is typed with two brackets after it and inside these brackets can be a string of text just like print:

	input('This is the structure of an input syntax')

The difference is that after the text is printed to the command line, before the code continues, it will wait for the user to enter information. Left just as it is, this information that the user input will just disappear. to be able to store this information and use it elsewhere we will need to implement another concept within the same line of text.

This syntax that can store a piece of information and be reused elsewhere is called a variable. Many programming language require the programmer to define what time of data will be stored in a variable (string, integer, float, etc) but python is dynamic and does not require this. A variable can be named whatever desired, but is recommended to use something short and descriptive of what information it will store. To use a variable to store information type the name of the variable followed by and equal with a space then the information that needs to be stored:

	variable = information

So in our case, we are trying to store the information that the user entered when we used the input syntax. We'll name the variable something that makes sense and follow it by the input syntax:

	wanna_play = input('Would you like to play the guessing game? (Enter Yes/No): ')

Now the message in the quotes will be displayed and the user will be able to enter a response which will then be stored in the 'wanna_play' variable.

	A pro tip that can help with troubleshooting a program is that the print syntax can be used to print what is inside a variable to make sure it contains what is expected. The print syntax can print many other bits of information as well but is beyond the scope of this tututorial.

We have now stored the users response but will ignore the variable for a moment and start working on the actual number guessing portion of the game before incorporating it.

The first part of having a number guessing game is to have a number to guess. To create a random number in python, there is a aptly named syntax called 'random'. However, this syntax is not built into python by default and we must use another concept first to 'import' this syntax. This is as simple as stating:

	import random

It is best to add all imports together at the top of the code for readability.

Now to use this syntax. let's say for this program we want to create a whole number between 1 and 10. We will call random and one of it's built in functions that creates an integer (a whole number) between to given values. A function of a class (which random is and will be explained in more detail later) can be called by typing the class following by a period then the function name:

	random.randint()

Notice the brackets at the end of the call. we have seen these previously with the print and input syntax. This is characteristic of any function. A function is a set of code that does a specific task. We will be creating our own later. The purpose of the brackets is to accept input or 'arguments' which will be used inside the code (Just like print and input accepted text so that it could then print that out to a command line). In the case of the randint function, we should add two numbers that we want our random integer to fall between separated by a comma:

	random.randint(1, 10)

This will create a whole number between 1 and 10 (to include 1 and 10). Now, just like when we used the input function, we can forget to store this number in a variable to use elsewhere:

	random_number = random.randint(1, 10)

Since we wanted to ask the player if they wanted to play or not after we greeted them, it was necessary to put that statement with it's variable to hold the answer after the greeting we created. But, in the case of the random_number variable, it is best to put it at the top of our main section of code. just like it is best for readability to put all import statements at the very top of a program, it is best for readability and management to create all variable at the beginning of the section of code it belongs to so that it is easy to find and alter if needed. That being said, our code so far look like this:

	import random


	random_number = random.randint(1, 10)

	print('Hello! Welcome to the game of guesses!')
	wanna_play = input('Would you like to play the guessing game? (Enter Yes/No): ')

Now that we have our random number, we need the player to take a guess. We will do this the same way we ask the player to play the game but this time asking for a number and storing the players response in a new variable:

	guess = input('Pick a number between 1 and 10: ')

Next we need to create the logic to figure out if the players guess matched the random number we created. To do this we need a new concept called an 'if' statement. An if statement will evaluate the given statement, and if true execute the line of code within the if block:

	if statement=True:
	    execute_code

Notice that the second line is indented. This is critical. White space is important in python and an if statemen will not work properly if this indention is not given. The preferred amount of indention for python is 4 space. So if we created an if statement to evaluate if the players guess matched the random number then congratulate the player on a good guess we could type:

	if guess == random_number:
	    print('Nice! You guessed it!')

We checked the variable 'guess' which holds the players guess against the variable random_number which holds the random number we generated. Note that we used two equals to compare, as a single equal is used to store values as we did when storing values in the variables.

We also need to account for when the player guessed wrong. We could add another if statement, but doing that means that the python interpreter would evaluate the first if statement we created, then even if the player  was correct, it would still evaluate our next if statement. In our case this would not break the program since if the guess and random number match, then they will not also not match, but in other cases it might be crucial that some blocks of code are skipped if they are no longer applicable. In the case that we need multiple if statements and we only want one of those blocks to run, we can use an 'elif' statement (short for else if). this must be used immediately after an initial if block and is identical except that 'if' is replaced with 'elif'. We could type:

	if guess == random_number:
	    print('Nice! You guessed it!')
	elif guess != random_number:
	    print('You lose!')

Notice that the elif starts back at the same level of indention as the original if statement. Also of notice is the the exclamation mark before the equal which contradicts it and means not equal to. However, this would be a pretty difficult game if the first wrong guess resulted in a loss, so let's give the player a hint when they are wrong. We'll need two elif statements for this, one to hint that the correct number is higher than their guess, and one to hint that the number is lower than their guess:

	if guess == random_number:
	    print('Nice! You got it!')
	elif guess > random_number:
	    print('It's lower.')
	elif guess < random_number:
	    print('It's higher.')

If you remember from math, the greater than and less than signs are used to compare whether the guess is higher or lower than the random number. Each statement will be evaulated from top to bottom and once one statement is true, the code within will be run and the remainder of the if/elif statements in this block will be skipped.

There is also a third form of the if statement call 'else' which can be used as a catch all at the end of if/elif block if you what a block of code to run if no other given statement is true. We will not use it in our code just yet but to use it jsut type 'else' instead of if or elif and then a colon with no comparison statement:

	if something == something_else:
	    # do this
	elif other_something == other_something_else:
	    # or do this
	else:
	    # else do this line of code if nothing else is true

It's also overdue to mention a concept used in the example above. The comment. Anytime a number or pound sign is used, everythin type from there to the end of the line is ignored by the interpretor. The purpose of this is exactly what the name of the concept suggests. This allow a programmer to add comments that are human readable, but do not effect the code so important information about why a particular piece of code was created that way that is was can be express, either to remind the programmer in the future, or to inform other programmers that might also be reading the code. This is an important and often underutilized syntax for any programming lanuage. A '#' only work for a single line at a time, so if a multi line comment is desire, three single or double quotes before and after can also be used and everything inbetween will be ignored by the interpreter:

	'''All of the lines of text between
	these pairs of three quotes will be
	ignored by the python interpreter.'''

Now that that bit of important tip is out of the way, back to the guessing game.

If you attempt to  run the code that we've created so far and attempt to play it yourself, you may have noticed an issue. If you are wrong, you will be given a hint, but then you are not given a chance to guess again! How do we handle this? Should we create a whole list of these if block? Do we need 10 of them for every possible number in our number range? NO! absolutely not, that might work, but would be sloppy and time consuming. Instead, python has a way to repeat lines of code for a desire number of times. This is a loop. There are two forms of a loop, a for loop and a while loop. A for loop will continuously run a block of code based on an iteration of a given value, and the while loop will continue running a block of code as long as the given statement is true. The while loop will be used here so that we can continue allowing the player to guess for as long as they want to continue playing.

Since we already learned the if statement. I while loop should be easy. replace the 'if' with 'while' and the rest is the same:

	while statement == True:
	    # run this code again and again

The code indented within the while loop will run from top to bottom, then, as long as the given statement is still true, the code will run again. each time the block of code within the will loop finishes, the while statement will be evaluated again for truth before rerunning. Once the statement is found to be false, the block will be skipped and the program will continue from the following code. Since we want to loop our guessing logic for as long as the player wants to continue playing and, if you remember from earlier, we already asked if the player wanted to play, we will use that bit of information with our while loop:

	while wanna_play.lower() == 'yes':
	    if guess == random_number:
	    ...

If the player answered yes when prompted, this loop will rerun over and over. If you are wondering what the '.lower()' tacked on to the end of the wanna_play variable is, it temporarily alter the variable before while evaluates it. Because it has two brackets that means it is a function. Every variable that contains a string inherets many function by default (another concept that is beyond the scope of this lesson) so we are able to use this particular function to tranform every character in the variable string to a lower case letter in case the player answered with any upper case characters, as python is case sensitive.

There is now another issue in this code that was created when we implemented the while loop. The loop will continue even when the guess is correct. We need a way to exit this loop and to exit this loop we need to change the wanna_play variable to something other that yes, so that the while comparison is false and the loop does not continue again. To do this, we will add a line within the while loop that will change the variable. The most obvious place would be once the player guesses correctly. We should have a fair grasp on how to assign values to variables now. What we can do is use the same syntax as when we originaly asked the player if they want to play and alter it to ask the player if they would like to play again

There is now another issue in this code that was created when we implemented the while loop. The loop will continue even when the guess is correct. We need a way to exit this loop and to exit this loop we need to change the wanna_play variable to something other that yes, so that the while comparison is false and the loop does not continue again. To do this, we will add a line within the while loop that will change the variable. The most obvious place would be once the player guesses correctly. We should have a fair grasp on how to assign values to variables now. What we can do is use the same syntax as when we originaly asked the player if they want to play and alter it to ask the player if they would like to play again:

	if guess == random_number:
	   print('Nice! You guessed it!')
	   wanna_play = input('Would you like to play again? (Enter Yes/No) ')
	...

Now the player has an opportunity to decide whether to loop through again or end the game.

The game is more or less functional now. The rest of this tutorial will work on tying up loose ends and adding some flair while teaching some extra python concepts.

One major loose end that some may have noticed early on is what happens when the player says no when they are asked to play at the very start of the program. If you tested the already or test it now, you will find that the program simply ends. This is the same after the player guesses correct and chooses not to continue playing. We need a cleaner end to the game when the player is done playing. Adding a final message after and outside the while loop could do the job, but while loops also work with 'else' statements like the if statement does. It works exactly the same and will be run whenever the while loop does not run at all or does not continue to run:

	while wanna_play.lower() == 'yes':
	    ...
	else:
	    print('That's ok, have a good one!')

Since there is no code after this else statement, the program will end, but this is a much cleaner end that before.

There is another issue with our code than many of you probably have not realized. We also ask the player to input a number between 1 adn 10, but in reality, the player could input literally anything, any number or even letters. We need to treat these two possibilities in two separate ways. We will start with the simpler solution first. 

We asked the player to guess between 1 and 10, but what if they guess higher or lower? We fix this in the same way that handled the other incorrect guesses. We'll add another 'elif' statement somewhere between the 'if' and 'else' statements:

	if guess == random_number:
	    ...
	elif guess < 1 or guess > 10:
	    print('Please guess a number between 1 and 10.')
	...
	else:
	    ...

Now the player will be will be reminded to guess between 1 and 10 and the loop will restart and ask for another guess. The or between the two statements means that if one or the other statement is true then the code will be executed. 'and' can also be used if both statements must be true. 'and', 'or', as well as another other mathmatical comparative symbol can be used in succession as needed.

The more involved fix would be if the player inputs a something other than a whole number.If the player does this by enter 'five' or '4.5' python cannot compare a string or a float against an integer and will give an error exit the program. Just because the player made a mistake in entering a value that the program was not intended to handle does not mean the program should break. This is how you run into 'bugs' in a program. sometimes this is due to unintended user input, or sometimes it is a mistake in the code itself. So it if very important to consider not only how you expect a user to interact with your program, but also consider how they could mistakenly or intentional do something unintended and try your best to account for that. This will be a multi step process to account for this possibility.

first, we will attempt to inforce the that input given and stored in the guess variable is an integer. To do this we will inbed the input function into another function named 'int':

	guess = int(input('Pick a number between 1 and 10: '))

This function attempts to tranform the input into an integer before storing the value in guess. if it cannot it will give an error. This will save our if statements from trying to evaluate a non integer against the integer random_number, but it will still exist our program if the player does enter a non integer and causes an error. We need to handle the error gracefully and allow the program to continue.

We will first learn the 'try' syntax. This syntax will attempt or try to run the nested code, and if it fails with an error it will jump to an 'except' statement that follows it:

	try:
	    # code with the potential for an error
	except:
	    # what to do in the case of an error

Once the except block is handled, it will then loop and try again and continue this loop until the code within the try block succeeds. Now, it is poor practice to use the 'except' statement by itself, because it would then handle any error that occurs in the same way and will make it difficult to debug a program. Instead it is best to define what type of error we want this except statement to handle, in this case a ValueError:

	except ValueError as err:
		# cooe to handle a ValueError

Now this except statement will only handle a ValueError and other error can still break the program, but will give you an error message to help debug it. The 'as err' is defining 'err' as an argument that this block can accept similiar to a function. to explain how this work, let's go back to the 'elif' statement that handle if the player guesses above or below the expected range.

Instead of using the print statement to remind the player of the expected guess, we will raise a value error by calling 'ValueError' as if it were a function and place our message in the brackets:

	if guess < 1 or guess > 10:
	    raise ValueError('please guess a number between 1 and 10.')
	...

Now if the player guess above or below the range, the message we provided will be past to the ValueError exception as the variable 'err' which we will use in a moment. Any other error that python categorizes as a 'ValueError' will also be handle by this except statement with the error message stored within the 'err' variable.

Since this except statement will now properly be past all 'ValueError's, we need to define what we will do. Within the block, let's inform the player that they input a non valid value, then we can print 'err' which will be either the message we past if it is out of range, or a python generated error message:

	try:
	   ...
	except ValueError as err:
	   print('Oh no! That is not a valid value. Try again...')
	   print(err)

An error message from python might be a little cryptic, but hopefull is enough to steer the player in the right direction. multiple except statements can be used to handle different types of errors or even custom create ones that can be called when necessary, but we will not take the time to make extra except statements for this tutorial. Also note that we have now used a variable in a print statement for the first time, so feel free to test what happens when that runs.

The last small grievance to deal with is the fact that since the random number was generated at the very  beginning of our program and not within the while loop it does not change if the player choose to play another game after guessing correctly. What the fun in trying to guess that same number over and over? However, if we move the call the 'randint' to the start of the while loop, a new number will be set every time the while statement loops. We need to put we need to leave the current random_number where it is so that it is created before the player starts guessing, but we also need to assign a new random number to it when the player choose to continue playing.

The perfect place for this is within the 'if' statement for when the player guesses correctly:

	if guess == random_number:
	    print('Nice! You guessed it!')
	    random_number = random.randint(1, 10))
	...

The only time this block of code will be executed is when the player has guess to correct number and is given the opportunity to play again or quit, so we do not have to worry about the number changing mid game. This is now our full code up to this point:

	import random

	random_number = random.randint(1, 10)
	print("Hello! Welcome to the game of guesses!")
	wanna_play = input("Would you like to play the guessing game? (Enter Yes/No): ")
	
	while wanna_play.lower() == "yes":
	    try:
	        guess = int(input("Pick a number between 1 and 10: "))
	        if guess == random_number:
	            print("Nice! You guessed it!")
	            random_number = random.randint(1, 10)
	            wanna_play = input("Would you like to play again? (Enter Yes/No) ")
	        elif guess > random_number:
	            print("It's lower")
	        elif guess < random_number:
	            print("It's higher")
	        elif guess < 1 or guess > 10:
	            raise ValueError("Please guess a number between 1 and 10.")
	    except ValueError as err:
	        print("Oh no!, that is not a valid value. Try again...")
	        print(err)
	else:
	    print('That's ok! Have a good one!')
 
Our program is fully functional and robust now. It could be used as is, but there are some extra addition that could make the game a little more addicting. This can be seen in the example python program and a further instruction to explain all the concepts will be in the works.
