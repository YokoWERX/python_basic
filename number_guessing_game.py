'''Number Guessing Game'''


import random


class Score:
    '''class representing a scoreboard object'''
    def __init__(self):
        self.high_score = []

    def show_score(self, attempts):
        if len(self.high_score) <= 0:
            print('There is currently no high score, its yours for the taking!')
            self.high_score.append(attempts)
        elif attempts < self.high_score[0]:
            self.high_score.append(attempts)
            print(f'You reached a new high score of only {attempts} attempts!')
        else:
            print(f'The current high score is {self.high_score[0]} attempts')


def start_game():
    attempts = 0
    score = Score()
    random_number = random.randint(1, 10)
    print("Hello traveler! Welcome to the game of guesses!")
    wanna_play = input("Would you like to play the guessing game? (Enter Yes/No): ")

    while wanna_play.lower() == "yes":
        try:
            guess = int(input("Pick a number between 1 and 10: "))
            if guess < 1 or guess > 10:
                raise ValueError("Please guess a number between 1 and 10.")
            elif guess > random_number:
                print("It's lower")
            elif guess < random_number:
                print("It's higher")
            else:
                print("Nice! You guessed it!")
                print(f"It took you {attempts} attempts.")
                score.show_score(attempts)
                attempts = 0
                random_number = random.randint(1, 10)
                wanna_play = input("Would you like to play again? (Enter Yes/No) ")
        except ValueError as err:
            print("Oh no!, that is not a valid value. Try again...")
            print(err)

        attempts += 1
    else:
        print("That's cool, have a good one!")


if __name__ == '__main__':
    start_game()
