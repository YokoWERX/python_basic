"""explanation of how to create the Mad Libs generator program"""

# Create the base story.
#------------------------------------------
#Be kind to your noun-footed p_noun
#For a duck may be somebody's noun2,
#Be kind to your p_noun in place
#Where the weather is always adjective.
#
#You may think that this is the noun3,
#Well it is.
#------------------------------------------

# turn into print statements (teaches print syntax)
print ("------------------------------------------")
print ("Be kind to your noun-footed p_noun")
print ("For a duck may be somebody's noun2,")
print ("Be kind to your p_noun in place")
print ("Where the weather is always adjective.")
print ()
print ("You may think that this is the noun3,")
print ("Well it is.")
print ("------------------------------------------")

# Create variable to hold changable words (teaches variables)
noun = 
p_noun = 
noun2 = 
place = 
adjective = 
noun3 = 
