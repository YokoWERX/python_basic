'''Mad Libs Generator'''

# set variable to hold number of loops
loop = 1
# loop back to this point once code finishes
while (loop < 10):
    # all the questions to ask user
    noun = input('Choose a noun: ')
    p_noun = input('Choose a plural noun: ')
    noun2 = input('Choose a noun: ')
    place = input('Name a place: ')
    adjective = input('Choose an adjective (Describing word): ')
    noun3 = input('Choose a noun: ')

    # display story based on users input
    print ("------------------------------------------")
    print (f"Be kind to your {noun}-footed {p_noun}")
    print (f"For a duck may be somebody's {noun2},")
    print (f"Be kind to your {p_noun} in {place}")
    print (f"Where the weather is always {adjective}.")
    print ()
    print (f"You may think that this is the {noun3},")
    print (f"Well it is.")
    print (f"------------------------------------------")
    # increment loop variable
    loop += 1
