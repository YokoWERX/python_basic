# python_basic

A basic introduction to the Python programming language

There are two programs to teach multiple fundamentals of the Python programming language. The Mad Libs Generator is very quick to type and teach a few very basic concepts. The number guessing game is a bit longer and expands on the previous concepts as well as adding many others. Each program has the finish program for reference and an accompanying instruction file to explain all the concepts.
